﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VendingMachine;

namespace HW_1_Coke_Machine
{
    class Program
    {
        static void Main(string[] args)
        {
            CokeMachine cokeMachine = new CokeMachine();

            string response = "none";
            while (response.ToUpper() != "Q")
            {
                Console.WriteLine (
                    "Please type P to insert a dollar,\n" +
                    "or B to buy a Coke,\n" +
                    "or R to get all your money back,\n" +
                    "or Q to quit this program."
                    );
                response = Console.ReadLine();

                if (String.IsNullOrEmpty(response))
                {
                    Console.WriteLine("Please enter a valid command.");
                }
                else
                {
                    switch (Convert.ToChar(response.ToUpper().Substring(0,1)))
                    {
                        case 'P':
                            cokeMachine.AcceptCash();
                            Console.WriteLine("Thank you, you now have ${0}.00.", cokeMachine.Cash);
                            break;

                        case 'B':
                            switch (cokeMachine.BuyCoke())
                            {
                                case CokeMachine.Success:
                                    Console.WriteLine("Thank you for your purchase, you have ${0}.00 left", cokeMachine.Cash);
                                    break;

                                case CokeMachine.NoBottles:
                                    Console.WriteLine("Sorry, the machine is empty, enter an R to get your money back.");
                                    break;

                                case CokeMachine.NoCash:
                                    Console.WriteLine("Sorry, you need to insert a dollar.");
                                    break;

                                case CokeMachine.NoBottlesNoCash:
                                    Console.WriteLine("Sorry, the machine is empty, and you have no money left in the machine.");
                                    break;

                                default:
                                    Console.WriteLine("Internal error: BuyCoke() returned invalid value.");
                                    break;
                            }

                            break;

                        case 'R':
                            int refund = cokeMachine.GiveRefund();
                            Console.WriteLine("Here is your ${0}.00.", refund);
                            break;

                        case 'Q':
                            break;

                        default:
                            Console.WriteLine("Invalid input: \"{0}\"", response);
                            break;
                    }
                }

                Console.WriteLine();
            }
        }
    }
}
