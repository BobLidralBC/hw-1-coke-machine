﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    public class CokeMachine
    {
        public const int Success = 0;
        public const int NoBottles = 1;
        public const int NoCash = 2;
        public const int NoBottlesNoCash   = 3;

        private int bottleCount;

        private int cash;

        public int BottleCount { get { return bottleCount; } }

        public int Cash { get { return cash; } }

        public CokeMachine()
        {
            bottleCount = 5;
            cash = 0;
        }

        public void AcceptCash()
        {
            cash++;
        }

        public int BuyCoke()
        {
            if (cash > 0)
            {
                if (bottleCount > 0)
                {
                    bottleCount--;
                    cash--;
                    return Success;
                }
                else
                {
                    return NoBottles;
                }
            }
            else
            {
                if (bottleCount > 0)
                {
                    return NoCash;
                }
                else
                {
                    return NoBottlesNoCash;
                }
            }
        }

        public int GiveRefund()
        {
            int refund = cash;

            cash = 0;
            return refund;
        }
    }
}
